
package com.mg.swagger.mg.ui.demo.web.web.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.web.param.RecursiveParam;
import com.mg.swagger.mg.ui.demo.web.web.param.UserIdParam;
import com.mg.swagger.mg.ui.demo.web.web.vo.RecursiveVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 多层循环例子控制器
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "多层循环例子控制器", tags = "多层循环例子控制器")
@RestController
@RequestMapping("/model/recursive")
public class RecursiveModelController {
	
	@PostMapping("/param")
	@ApiOperation(value = "参数是表单的例子", response = RecursiveVo.class)
	public ResponseJson param(UserIdParam param) {
		return ErpResponseJson.ok();
	}

	@PostMapping("/body")
	@ApiOperation(value = "参数是body的例子", response = RecursiveVo.class)
	public ResponseJson body(@RequestBody RecursiveParam param) {
		return ErpResponseJson.ok();
	}

}
