package com.mg.swagger.mg.ui.demo.web.openApi.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 客户信息
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class CaseInfoVo {

	@ApiModelProperty(value = "用户名字")
	private String userName;

	@ApiModelProperty(value = "年龄")
	private Integer userAge;

	@ApiModelProperty(value = "性别，0=女 1=男")
	private Integer userSex;

	public CaseInfoVo(String userName, Integer userAge, Integer userSex) {
		this.userName = userName;
		this.userAge = userAge;
		this.userSex = userSex;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserAge() {
		return userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	public Integer getUserSex() {
		return userSex;
	}

	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}

}
