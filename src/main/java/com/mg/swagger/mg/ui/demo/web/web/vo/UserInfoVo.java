package com.mg.swagger.mg.ui.demo.web.web.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户信息
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class UserInfoVo {

	@ApiModelProperty(value = "用户名字")
	private String userName;

	@ApiModelProperty(value = "年龄")
	private Integer userAge;

	@ApiModelProperty(value = "性别，0=女 1=男")
	private Integer userSex;

	@ApiModelProperty(value = "学校信息")
	private SchoolInfo schoolInfo;

	public static class SchoolInfo {
		@ApiModelProperty(value = "学校名字")
		private String name;
		@ApiModelProperty(value = "学校地址")
		private String addr;
		@ApiModelProperty(value = "学校电话")
		private String phone;

		public SchoolInfo(String name, String addr, String phone) {
			this.name = name;
			this.addr = addr;
			this.phone = phone;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddr() {
			return addr;
		}

		public void setAddr(String addr) {
			this.addr = addr;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}
	}

	public UserInfoVo(String userName, Integer userAge, Integer userSex) {
		this.userName = userName;
		this.userAge = userAge;
		this.userSex = userSex;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserAge() {
		return userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	public Integer getUserSex() {
		return userSex;
	}

	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}

	public SchoolInfo getSchoolInfo() {
		return schoolInfo;
	}

	public void setSchoolInfo(SchoolInfo schoolInfo) {
		this.schoolInfo = schoolInfo;
	}

}
