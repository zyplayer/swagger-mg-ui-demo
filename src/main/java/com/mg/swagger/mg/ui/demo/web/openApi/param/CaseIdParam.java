package com.mg.swagger.mg.ui.demo.web.openApi.param;

import io.swagger.annotations.ApiModelProperty;

/**
 * 数据类型和ID参数
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class CaseIdParam {

	@ApiModelProperty(value = "数据类型", required = true)
	private Integer caseType;

	@ApiModelProperty(value = "数据ID", required = true)
	private Integer caseId;
	
	@ApiModelProperty(value = "理由", required = true)
	private String reason;

	public Integer getCaseType() {
		return caseType;
	}

	public void setCaseType(Integer caseType) {
		this.caseType = caseType;
	}

	public Integer getCaseId() {
		return caseId;
	}

	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
