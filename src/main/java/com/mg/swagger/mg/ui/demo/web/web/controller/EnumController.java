
package com.mg.swagger.mg.ui.demo.web.web.controller;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ExternalDocs;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.web.param.EnumParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 主要展示枚举的例子
 *
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "枚举例子控制器", tags = "枚举例子控制器")
@RestController
@RequestMapping("/body/enum")
public class EnumController {

	@PostMapping("/info")
	@ApiOperation(value = "测试枚举类型展示", response = EnumParam.class)
	public ResponseJson info(@RequestBody EnumParam param) {
		
		return ErpResponseJson.ok();
	}

}
