
package com.mg.swagger.mg.ui.demo.web.web.controller;


import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.web.param.UserIdParam;
import com.mg.swagger.mg.ui.demo.web.web.vo.UserInfoVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 主要展示Rest风格的例子
 * 没仔细研究过Rest风格、、暂且这样称呼吧
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "Rest例子控制器", tags = "Rest例子控制器")
@RestController
@RequestMapping("/rest/user")
public class RestRequestController {

	@GetMapping("/{id}")
	@ApiOperation(value = "获取用户信息", response = UserInfoVo.class)
	public ResponseJson get(@PathVariable("id") Integer id) {
		return ErpResponseJson.ok(id);
	}
	
	@PostMapping("/{id}")
	@ApiOperation(value = "新增用户信息", response = UserInfoVo.class)
	public ResponseJson post(@PathVariable("id") Integer id, @RequestBody UserIdParam param) {
		return ErpResponseJson.ok(id);
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value = "修改用户信息", response = UserInfoVo.class)
	public ResponseJson put(@PathVariable("id") Integer id, @RequestBody UserIdParam param) {
		return ErpResponseJson.ok(id);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "删除用户信息", response = UserInfoVo.class)
	public ResponseJson delete(@PathVariable("id") Integer id) {
		return ErpResponseJson.ok(id);
	}

}
