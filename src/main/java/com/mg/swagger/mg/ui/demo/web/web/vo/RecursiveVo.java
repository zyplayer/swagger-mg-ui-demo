package com.mg.swagger.mg.ui.demo.web.web.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 递归对象测试返回值
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class RecursiveVo {

	@ApiModelProperty(value = "用户类型", required = true)
	private Integer userType;

	@ApiModelProperty(value = "用户ID", required = true)
	private Integer userId;

	@ApiModelProperty(value = "递归对象", required = true)
	private RecursiveVo recursiveParam;

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public RecursiveVo getRecursiveParam() {
		return recursiveParam;
	}

	public void setRecursiveParam(RecursiveVo recursiveParam) {
		this.recursiveParam = recursiveParam;
	}

}
