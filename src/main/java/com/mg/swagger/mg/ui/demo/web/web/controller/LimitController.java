
package com.mg.swagger.mg.ui.demo.web.web.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.web.param.UserIdParam;
import com.mg.swagger.mg.ui.demo.web.web.vo.Limit2Vo;
import com.mg.swagger.mg.ui.demo.web.web.vo.LimitVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 变态测试例子控制器
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "变态测试例子控制器", tags = "变态测试例子控制器")
@RestController
@RequestMapping("/model/bt")
public class LimitController {
	
	@PostMapping("/btInterface")
	@ApiOperation(value = "变态测试例子", response = LimitVo.class)
	public ResponseJson btInterface(UserIdParam param) {
		return ErpResponseJson.ok();
	}
	
	@PostMapping("/btInterface2")
	@ApiOperation(value = "变态测试例子，我的备注是很长很长很长很长很长很长很长很长很长很长很长的！", response = Limit2Vo.class)
	public ResponseJson btInterface2(UserIdParam param) {
		return ErpResponseJson.ok();
	}

}
