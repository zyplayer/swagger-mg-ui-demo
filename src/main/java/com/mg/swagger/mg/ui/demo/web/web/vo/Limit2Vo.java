package com.mg.swagger.mg.ui.demo.web.web.vo;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;

/**
 * 递归对象测试参数
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class Limit2Vo {

	@ApiModelProperty(value = "用户类型", required = true)
	private Integer userType;

	@ApiModelProperty(value = "用户ID", required = true)
	private Integer userId;

	@ApiModelProperty(value = "对象", required = true)
	private Limit2Vo limitVo;

	@ApiModelProperty(value = "列表")
	private List<Limit2Vo> limitList1;

	@ApiModelProperty(value = "列表")
	private List<List<Limit2Vo>> limitList2;

	@ApiModelProperty(value = "列表")
	private Map<String, Limit2Vo> limitList3;

	@ApiModelProperty(value = "列表")
	private List<Map<String, Limit2Vo>> limitList4;

	@ApiModelProperty(value = "列表")
	private List<String> stringList;

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Limit2Vo getLimitVo() {
		return limitVo;
	}

	public void setLimitVo(Limit2Vo limitVo) {
		this.limitVo = limitVo;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> stringList) {
		this.stringList = stringList;
	}

	public List<Limit2Vo> getLimitList1() {
		return limitList1;
	}

	public void setLimitList1(List<Limit2Vo> limitList1) {
		this.limitList1 = limitList1;
	}

	public List<List<Limit2Vo>> getLimitList2() {
		return limitList2;
	}

	public void setLimitList2(List<List<Limit2Vo>> limitList2) {
		this.limitList2 = limitList2;
	}

	public Map<String, Limit2Vo> getLimitList3() {
		return limitList3;
	}

	public void setLimitList3(Map<String, Limit2Vo> limitList3) {
		this.limitList3 = limitList3;
	}

	public List<Map<String, Limit2Vo>> getLimitList4() {
		return limitList4;
	}

	public void setLimitList4(List<Map<String, Limit2Vo>> limitList4) {
		this.limitList4 = limitList4;
	}

}
