package com.mg.swagger.mg.ui.demo.web.web.vo;

import java.util.List;
import java.util.Map;

import com.mg.swagger.mg.ui.demo.web.web.param.UserIdParam;

import io.swagger.annotations.ApiModelProperty;

/**
 * 递归对象测试参数
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class LimitVo {

	@ApiModelProperty(value = "用户类型", required = true)
	private Integer userType;

	@ApiModelProperty(value = "用户ID", required = true)
	private Integer userId;

	@ApiModelProperty(notes="单个对象展示的notes信息，value和notes只能写一个，swagger会判断有value就不会解析notes了", required = true)
	private LimitVo limitVo;

	@ApiModelProperty(value = "一层的列表")
	private List<UserIdParam> limitList1;

	@ApiModelProperty(value = "两层的列表")
	private List<List<UserIdParam>> limitList2;

	@ApiModelProperty(value = "Map的对象")
	private Map<String, UserIdParam> limitList3;

	@ApiModelProperty(value = "Map的对象列表")
	private List<Map<String, UserIdParam>> limitList4;

	@ApiModelProperty(value = "字符串列表")
	private List<String> stringList;

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public LimitVo getLimitVo() {
		return limitVo;
	}

	public void setLimitVo(LimitVo limitVo) {
		this.limitVo = limitVo;
	}

	public List<String> getStringList() {
		return stringList;
	}

	public void setStringList(List<String> stringList) {
		this.stringList = stringList;
	}

	public List<UserIdParam> getLimitList1() {
		return limitList1;
	}

	public void setLimitList1(List<UserIdParam> limitList1) {
		this.limitList1 = limitList1;
	}

	public List<List<UserIdParam>> getLimitList2() {
		return limitList2;
	}

	public void setLimitList2(List<List<UserIdParam>> limitList2) {
		this.limitList2 = limitList2;
	}

	public Map<String, UserIdParam> getLimitList3() {
		return limitList3;
	}

	public void setLimitList3(Map<String, UserIdParam> limitList3) {
		this.limitList3 = limitList3;
	}

	public List<Map<String, UserIdParam>> getLimitList4() {
		return limitList4;
	}

	public void setLimitList4(List<Map<String, UserIdParam>> limitList4) {
		this.limitList4 = limitList4;
	}

}
