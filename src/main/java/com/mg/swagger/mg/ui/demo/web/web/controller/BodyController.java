
package com.mg.swagger.mg.ui.demo.web.web.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.web.param.FillTypeParam;
import com.mg.swagger.mg.ui.demo.web.web.param.ParamTypeParam;
import com.mg.swagger.mg.ui.demo.web.web.param.UserIdParam;
import com.mg.swagger.mg.ui.demo.web.web.vo.UserInfoVo;
import com.mg.swagger.mg.ui.demo.web.web.vo.UserInfoVo.SchoolInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 主要展示数据放在body里面的例子
 * 
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "Body参数例子控制器", tags = "Body参数例子控制器")
@RestController
@RequestMapping("/body/user")
public class BodyController {

	@PostMapping("/info")
	@ApiOperation(value = "通过ID和类型获取用户信息", response = UserInfoVo.class)
	public ResponseJson info(@RequestBody UserIdParam param) {
		UserInfoVo userInfoVo = new UserInfoVo("暮光：城中城", 24, 1);
		userInfoVo.setSchoolInfo(new SchoolInfo("四川工程职业技术学院", "德阳市泰山南路二段801号", "0838-2651110"));
		return ErpResponseJson.ok(userInfoVo);
	}

	@DeleteMapping("/delete")
	@ApiOperation(value = "删除用户信息", response = UserInfoVo.class)
	public ResponseJson delete(@RequestBody UserIdParam param) {
		UserInfoVo userInfoVo = new UserInfoVo("暮光：城中城", 24, 1);
		userInfoVo.setSchoolInfo(new SchoolInfo("四川工程职业技术学院", "德阳市泰山南路二段801号", "0838-2651110"));
		return ErpResponseJson.ok(userInfoVo);
	}
	
	@PostMapping("/type")
	@ApiOperation(value = "举例所有数据类型", response = ParamTypeParam.class)
	public ResponseJson type(@RequestBody ParamTypeParam param) {
		return ErpResponseJson.ok();
	}
	
	@PostMapping("/fillType")
	@ApiOperation(value = "依据数据类型自动填充", response = FillTypeParam.class)
	public ResponseJson fillType(@RequestBody FillTypeParam param) {
		return ErpResponseJson.ok();
	}

}
