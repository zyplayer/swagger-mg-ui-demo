
package com.mg.swagger.mg.ui.demo.web.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

/**
 * 主要展示数据放在表单里面的例子
 * 
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "表单参数例子控制器", tags = "表单参数例子控制器")
@RestController
@RequestMapping("/form/param")
public class FormParamController {

	@ApiImplicitParams({
		@ApiImplicitParam(value = "订单状态，0=已提交 1=已抢单", name = "orderStatus", dataType = "Byte", required = true, paramType="query", defaultValue="1"),
		@ApiImplicitParam(value = "订单状态，0=已提交 1=已抢单", name = "pageNum", dataType = "Byte", required = true, paramType="query", defaultValue="2")
	})
	@GetMapping("/info")
	ResponseJson getInfoList(Byte orderStatus, Integer pageNum) {
		
		return ErpResponseJson.ok();
	}

}
