
package com.mg.swagger.mg.ui.demo.framework.config;

import com.google.common.base.Predicate;
import io.swagger.annotations.ExternalDocs;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 支持分组的配置，例如需要给第三方文档、内部使用的文档等分组，上线后只开启第三方文档，内部文档关闭
 * @author 暮光：城中城
 * @since 2018年8月19日
 */
@Component
@EnableSwagger2
public class SwaggerConfiguration {
	
	// 划重点！！：读取配置文件中是否是线上，如果是线上则关闭文档功能，否则所有接口全部暴露了！
	//Predicate<String> paths = AppConfig.isOnline() ? PathSelectors.none() : PathSelectors.any();
	Predicate<String> paths = false ? PathSelectors.none() : PathSelectors.any();

	@Bean(value = "serverApi")
	public Docket serverApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(serverApiInfo())
				.groupName("服务Api")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.mg.swagger.mg.ui.demo.web.web"))
				.paths(paths)
				.build();
	}
	
	@Bean(value = "openApi")
	public Docket openApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(openApiInfo())
				.groupName("开放接口")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.mg.swagger.mg.ui.demo.web.openApi"))
				.paths(paths)
				.build();
	}

	private ApiInfo openApiInfo(){
		return new ApiInfoBuilder()
			.title("开放接口")
			.description("swagger-mg-ui-demo接口文档")
			.termsOfServiceUrl("http://doc.zyplayer.com/")
			.contact(new Contact("暮光：城中城", "http://doc.zyplayer.com/", "806783409@qq.com"))
			.version("1.0")
			.build();
	}

	private ApiInfo serverApiInfo() {
		return new ApiInfoBuilder()
			.title("服务Api")
			.description("swagger-mg-ui-demo接口文档")
			.termsOfServiceUrl("http://doc.zyplayer.com/")
			.contact(new Contact("暮光：城中城", "http://doc.zyplayer.com/", "806783409@qq.com"))
			.version("1.0")
			.build();
	}

}
