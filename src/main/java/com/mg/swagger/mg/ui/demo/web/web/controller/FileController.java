
package com.mg.swagger.mg.ui.demo.web.web.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 文件上传例子
 * 
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "文件上传例子控制器", tags = "文件上传例子控制器")
@RestController
@RequestMapping("/upload/file")
public class FileController {

	@PostMapping("/single")
	@ApiOperation(value = "单文件上传")
	public ResponseJson single(@RequestParam(value = "files") MultipartFile files, @RequestParam(value = "title") String title) {
		
		return ErpResponseJson.ok();
	}

	@PostMapping("/multipart")
	@ApiOperation(value = "多文件上传")
	public ResponseJson multipart(@RequestParam(value = "file") MultipartFile[] file, @RequestParam(value = "title") String title) {
		
		return ErpResponseJson.ok();
	}

}
