
package com.mg.swagger.mg.ui.demo.web.openApi.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mg.swagger.mg.ui.demo.framework.json.ErpResponseJson;
import com.mg.swagger.mg.ui.demo.framework.json.ResponseJson;
import com.mg.swagger.mg.ui.demo.web.openApi.param.CaseIdParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "客户信息控制器", tags = "客户信息控制器")
@RestController
@RequestMapping("/openApi/case")
public class OpenApiCaseController {

	@GetMapping("/info")
	@ApiOperation(value = "通过ID和类型获取数据信息")
	public ResponseJson info(CaseIdParam param) {

		return ErpResponseJson.ok(param);
	}
	
	@PostMapping("/create")
	@ApiOperation(value = "通过ID和类型增加数据信息")
	public ResponseJson create(HttpServletResponse response, CaseIdParam param) {
		response.addCookie(new Cookie("testCookieName", "testCookieValue"));
		return ErpResponseJson.ok(param);
	}
	
	@PutMapping("/update")
	@ApiOperation(value = "通过ID和类型修改数据信息")
	public ResponseJson update(CaseIdParam param) {
		
		return ErpResponseJson.ok(param);
	}
	
	@DeleteMapping("/delete")
	@ApiOperation(value = "通过ID和类型删除数据信息")
	public ResponseJson delete(CaseIdParam param) {
		
		return ErpResponseJson.ok(param);
	}

}
