package com.mg.swagger.mg.ui.demo.web.web.param;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户类型和ID参数
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class ParamTypeParam {

	@ApiModelProperty(value = "用户类型", required = true)
	private Integer userType;

	@ApiModelProperty(value = "用户ID", required = true)
	private String userId;

	@ApiModelProperty(value = "年龄", required = true)
	private Long ages;

	@ApiModelProperty(value = "性别", required = true)
	private Boolean sex;

	@ApiModelProperty(value = "身高", required = true)
	private Double height;

	@ApiModelProperty(value = "身高", required = true)
	private Float weight;

	@ApiModelProperty(value = "实名认证", required = true)
	private Short userRight;

	@ApiModelProperty(value = "用户状态", required = true)
	private Byte userStatus;

	@ApiModelProperty(value = "创建时间", required = true)
	private Date creationTime;

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getAges() {
		return ages;
	}

	public void setAges(Long ages) {
		this.ages = ages;
	}

	public Boolean getSex() {
		return sex;
	}

	public void setSex(Boolean sex) {
		this.sex = sex;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public Short getUserRight() {
		return userRight;
	}

	public void setUserRight(Short userRight) {
		this.userRight = userRight;
	}

	public Byte getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Byte userStatus) {
		this.userStatus = userStatus;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

}
