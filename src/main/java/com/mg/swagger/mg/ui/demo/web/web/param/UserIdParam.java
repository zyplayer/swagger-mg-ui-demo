package com.mg.swagger.mg.ui.demo.web.web.param;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户类型和ID参数
 * 
 * @author 暮光：城中城
 * @since 2018年8月7日
 */
public class UserIdParam {

	@ApiModelProperty(value = "用户类型", required = true)
	private Integer userType;

	@ApiModelProperty(value = "用户ID", required = true)
	private Integer userId;

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
