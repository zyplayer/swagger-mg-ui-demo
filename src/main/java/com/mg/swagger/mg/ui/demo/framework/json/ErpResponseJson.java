package com.mg.swagger.mg.ui.demo.framework.json;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;

import io.swagger.annotations.ApiModelProperty;

/**
 * 返回数据格式
 * @author 暮光：城中城
 * @since 2018年8月19日
 */
public class ErpResponseJson implements ResponseJson {
	private static SerializeConfig mapping = new SerializeConfig();
	static {
		mapping.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
	}

	@ApiModelProperty(value = "业务处理成功或者失败 1：成功、0：失败 ")
	private int status;
	@ApiModelProperty(value = "状态码")
	private int errCode;
	@ApiModelProperty(value = "返回值说明")
	private String errMsg;
	@ApiModelProperty(value = "返回数据")
	private Object data;
	@ApiModelProperty(value = "总数")
	private Long total;
	@ApiModelProperty(value = "当前页数")
	private Integer pageNum;
	@ApiModelProperty(value = "每页条数")
	private Integer pageSize;
	@ApiModelProperty(value = "总页数")
	private Integer totalPage;
	@ApiModelProperty(value = "当前服务器时间")
	private Date serverTime = new Date();

	public ErpResponseJson() {
		this.errCode = 200;
	}

	public ErpResponseJson(Object data) {
		this.setData(data);
		this.errCode = 200;
	}

	public ErpResponseJson(int errCode, String errMsg) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public ErpResponseJson(int errCode, String errMsg, Object data) {
		super();
		this.setData(data);
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public ErpResponseJson(int errCode) {
		super();
		this.errCode = errCode;
	}

	public <T> ErpResponseJson(List<T> data, Long total, Integer pageNum, Integer numPerPage) {
		super();
		this.errCode = 200;
		this.data = data;
		this.total = total;
		this.pageNum = pageNum;
		this.pageSize = numPerPage;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Date getServerTime() {
		return serverTime;
	}

	public void setServerTime(Date serverTime) {
		this.serverTime = serverTime;
	}

	/**
	 * 成功的返回方法
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 * @return
	 */
	public static ErpResponseJson ok() {
		return new ErpResponseJson(new JSONObject());
	}

	/**
	 * 成功的返回方法
	 * 
	 * @author 暮光：城中城
	 * @since 2018年8月7日
	 * @return
	 */
	public static ErpResponseJson ok(Object data) {
		return new ErpResponseJson(data);
	}

	public String toJson() {
		return JSON.toJSONString(this, mapping);
	}

	@Override
	public String toString() {
		return "DefaultResponseJson [errCode=" + errCode + ", errMsg=" + errMsg + ", data=" + data + "]";
	}

}
